# Documentation

### Documentación del proyecto integrador.

Incluye:

- Chronograma
- Documento general proyecto integrador (primer entregable).

- UML:

   - Diagrama de clases

   - Modelo entidad-relación

   - Modelo vista controlador

- Documento de requerimientos

    - Casos de uso.

    - Diagrama de clases.
